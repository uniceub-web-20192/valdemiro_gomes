package main

import ( "net/http"
		 "time" 
		 "log"
		 "io/ioutil"
		 "encoding/json"
		 "github.com/gorilla/mux")

type User struct {

	Email  string `json:email`
	Pass   string `json:pass` 
	BirthDate string `json:birthDate`
}


func main() {

	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", register).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	server := &http.Server{
			Addr       : "172.22.51.133:8082",
			IdleTimeout: duration,
			Handler    : r, 
	}

	log.Print(server.ListenAndServe())
}

func register(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)

	json.Unmarshal(body, &u)
	
	res.Write([]byte("{'message': 'success'}"))
}